﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Provider;
using Android.Content;
using Android.Runtime;
using Android.Graphics;
using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;

namespace HACKTUMv1
{
    [Activity(MainLauncher = true, Icon = "@drawable/ChaTV")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            Button camerabutton = FindViewById<Button>(Resource.Id.myButton);
            camerabutton.Click += FireCamera;
        }


        private void FireCamera(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            StartActivityForResult(intent, 1);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            try
            {
                byte[] bitmapData = LoadBitmap(data);
                string fileName = DateTime.Now + ".jpg";

                UploadFileToStorage(bitmapData, fileName);
                ClassificationAndSlack(fileName);
                StartActivity(typeof(Loading));
            }
            catch(Exception)
            {
                DispToast("Error!");
            }
        }

        private byte[] LoadBitmap(Intent data)
        {
            Bitmap bitmap = (Bitmap)data.Extras.Get("data");
            byte[] bitmapData = null;
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 80, stream);
                bitmapData = stream.ToArray();
            }

            return bitmapData;
        }

        private async void UploadFileToStorage(byte[] bitmapData, string fileName)
        {

            string accountName = "screenimages";
            string accountKey = "VYFoAHDCKn3c5t0E3/m+Hd60nrDBnnRyE+MArxVE805CrjoEZbZdkaPIq7TarJOY/8SExpZEIeF+2NYPdMcyww==";
            string containername = "paragtest";

            // Create storagecredentials object by reading the values from the configuration (appsettings.json)
            StorageCredentials storageCredentials = new StorageCredentials(accountName, accountKey);

            // Create cloudstorage account by passing the storagecredentials
            CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, true);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Get reference to the blob container by passing the name by reading the value from the configuration (appsettings.json)
            CloudBlobContainer container = blobClient.GetContainerReference(containername);

            // Get the reference to the block blob from the container
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            // Upload the file
            MemoryStream stream = new MemoryStream(bitmapData);
            await blockBlob.UploadFromStreamAsync(stream);

            //upload.Wait();
            stream.Close();
        }

        private async void ClassificationAndSlack(string fileName)
        {
            string teamId = "T82KP6K1T";

            // Open Azure Function URL and read result as channel name
            string url = "https://dispatcherfunc.azurewebsites.net/api/HttpTriggerCSharp1?code=6Laa68fgGEGGfYjwKhJ0dbhtbhTrOLlbapmPO0KaNuhWp7xZ8IqXsQ=="
                                                + "&path=../paragtest/" + fileName + ".jpg";
            string channelName = null;
            string channelId = null;
            double probability = 0;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                result = result.Replace("\"", "");
                string[] results = result.Split(',');
                channelName = results[0].Trim();
                channelId = results[2].Trim();
                probability = Double.Parse(results[1].Trim());
            }


            // Open corresponding channel in Slack
            if (channelName == null || channelName.Trim().Equals("") || probability < 0.4)
            {
                DispToast("Channel Not Identified");
                StartActivity(typeof(MainActivity));
            }
            else
            {
                String x = "Channel = " + channelName;
                DispToast(x);
                var uriString = Android.Net.Uri.Parse("slack://channel?team=" + teamId + "&id=" + channelId);
                var intent = new Intent(Intent.ActionView, uriString);
                StartActivity(intent);
            }
        }

        public void DispToast(string text)
        {
            Toast.MakeText(this, text, ToastLength.Long).Show();
        }

    }
}

